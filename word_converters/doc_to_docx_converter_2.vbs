'--------------------------------------
'Script to convert .doc to .docx files
'19.6.2017 Abhinav Y
'--------------------------------------
Const strPath = "C:\Users\ayalamanchili\Documents\Docs"
Dim objFSO
Set objFSO = CreateObject("Scripting.FileSystemObject")
convertDocToDocx (strPath)

' Comment out below line if you'd like to use this script in windows schedule task
WScript.Echo"Done."

Sub convertDocToDocx( str )
    Dim objWordApplication As New Word.Application
    Dim objWordDocument As Word.Document
    Dim objFolder, objSubFolder, objFile
    Set objFolder = objFSO.GetFolder(str)
    
    For Each objFile In objFolder.Files
        ' Convert DOC to DOCX
        If (Right(LCase(objFile.Name), 4) = ".doc") Then
        
            With objWordApplication
                Set objWordDocument = .Documents.Open(FileName:=strFolder & strFile, AddToRecentFiles:=False, ReadOnly:=True, Visible:=False)
    
                With objWordDocument
                    .SaveAs FileName:=strFolder & Replace(strFile, "doc", "docx"), FileFormat:=16
                    .Close
                End With
            End With
        ' Delete old Doc file(s)
            objFile.Delete (True)
        End If
 
    Next
    Set objWordDocument = Nothing
    Set objWordApplication = Nothing
    For Each objSubFolder In objFolder.SubFolders
        convertDocToDocx (objSubFolder.Path)
    Next

End Sub