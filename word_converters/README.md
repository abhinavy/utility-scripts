# Word Converters

We all hate Microsoft Word Especially the Old `1997/2003` `DOC` Formats. You are unable to open them in OSX `Pages`/`Number` or LInux `LibreOffice`/`OpenOffice`. Hence get your content in your friends work or home laptop with `Microsoft Office 2007 /2010/ 2012/365` Installed. 

These VBScripts will be interacting MS Word (I have tested on Word 2010)

# Usage

* Take a backup of the folder before proceeding. `D:\Documents\test`

* Set the following values in the scripts
``` powershell
sFolder = "D:\Documents\test" #The folder with documents
bRecursive = True ' True for recurssively converting subfolder
oWord.Visible = True 'True to open word in foreground and show files as recently opened.
```

* Run the `doc_to_docx_converter.vbs` for converting Word 97-2003 `DOC` format to Word 2007 `DOCX` format
```powershell
cscript.exe .\doc_to_docx_converter.vbs
```
* Run the `doc_to_pdf_converter.vbs` for converting Word 97-2003 `DOC` format to Adobe `PDF` format
```powershell
cscript.exe .\doc_to_pdf_converter.vbs
```

* Run the `docx_to_pdf_converter.vbs` for converting Word 2007 `DOCX` format to Adobe `PDF` format
```powershell
cscript.exe .\docx_to_pdf_converter.vbs
```

* Check the folder information for number of files before and after operation, by compairng with backup

* The script should not delete other files present in the folder.

![vbscript_execution](./commands.PNG)


# Reference

Refer the following API documentation for [SaveAs](https://msdn.microsoft.com/en-us/vba/word-vba/articles/document-saveas2-method-word) and [WdSaveFormat Enumeration](https://msdn.microsoft.com/en-us/vba/word-vba/articles/wdsaveformat-enumeration-word) when converting formats