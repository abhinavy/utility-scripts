'--------------------------------------
'Script to convert .doc to .pdf files
'19.6.2017 Abhinav Y
'--------------------------------------
bRecursive = True 'False
sFolder = "C:\Users\ayalamanchili\Documents\Docs"
Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oWord = CreateObject("Word.Application")
oWord.Visible = True 'False

Set oFolder = oFSO.GetFolder(sFolder)
ConvertFolder(oFolder)
oWord.Quit

' Comment out below line if you'd like to use this script in windows schedule task
WScript.Echo"Done."

Sub ConvertFolder(oFldr)
    For Each oFile In oFldr.Files
        If LCase(oFSO.GetExtensionName(oFile.Name)) = "doc" Then
            ' Convert DOC to PDF
            Set oDoc = oWord.Documents.Open(oFile.path)
            oWord.ActiveDocument.SaveAs Replace(oFile.path, ".doc", ".pdf") , 17
            oDoc.Close

            ' Delete old Doc file(s)
            oFile.Delete (True)
        End If
    Next

    If bRecursive Then
        For Each oSubfolder In oFldr.Subfolders
            ConvertFolder oSubfolder
        Next
    End If
End Sub