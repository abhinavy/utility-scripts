# utility-scripts
Useful scripts written in Python and Shell for special purposes


## Content ##
* [Shell Script](./ntfs.sh) to permanently attach NTFS drives as Read/Write in OSX 10+
* [Shell Script](./unlock_files.sh) to unlock file in External Drives (usually NTFS) so that they can be moved or copied
*  [Python Script](./match-tracker.py) to track Cricket Matches and Genertate a Windows Alert when wickets fall
*  [Python Script](./price-alert.py) to track Amazon Catalog price fluctuation( decreased by 1% than previous low/ set traget)
*  [VBScript(s)](./word_converters) for converting Word Document Formats (DOC,DOCX,PDF)


## TO DO ##

### Cricket Match Tracker ####
* Add support to cricinfo and cricbuzz
* Automatic slection of Ongoing Matches
* Notify at the End of Over or Six or Four
* Pause when there is no activity in score card or overs done for the day

### Price tracker ###
* Use Big Data Technologies for Predictive Analysis
* Support for other sites and catalog browsing